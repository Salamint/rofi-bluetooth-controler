#!/bin/python3.11

import asyncio
import sys
from bleak import BleakScanner


async def main():
    if len(sys.argv) < 2:
        return 2

    try:
        devices = await BleakScanner.discover()
    except OSError as e:
        return 1

    number_of_devices = len(devices)
    with open(sys.argv[1], "w") as file:
        for i, dev in enumerate(devices):
            if dev.name != dev.address.replace(":", "-"):
                file.write(f"{dev.address} {dev.name}")
                if i + 1 < number_of_devices:
                    file.write("\n")
                print(f"{dev.address} {dev.name}")
    return 0


if __name__ == '__main__':
    sys.exit(asyncio.run(main()))
