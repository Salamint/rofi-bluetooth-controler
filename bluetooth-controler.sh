#!/bin/zsh

pos="yes"
neg="no"

icon_active="󰂯"
icon_connected="󰂱"
icon_inactive="󰂲"
icon_scanning="󰂰"

declare -A icons_connection icons_pairing icons_bonding icons_trusting icons_blocking
icons_connection=(["$neg"]="󰌘 Connect" ["$pos"]="󰌙 Disconnect")
icons_pairing=(["$neg"]="󰠘 Pair" ["$pos"]="󱙄 Unpair")
icons_bonding=(["$neg"]=" Bond" ["$pos"]=" Unbond")
icons_trusting=(["$neg"]=" Trust" ["$pos"]=" Untrust")
icons_blocking=(["$pos"]=" Unblock" ["$neg"]="󰂭 Block")

declare -A icon_devices
icon_devices=(
	["audio-card"]=""
	["audio-headset"]=""
	["audio-headphones"]=""
	["camera-photo"]="󰄀"
	["camera-video"]=""
	["computer"]="󰇅"
	["input-gaming"]="󰊴"
	["input-keyboard"]="󰌌"
	["input-mouse"]="󰍽"
	["input-tablet"]="󰦧"
	["modem"]="󰀂"
	["multimedia-player"]="󰎇"
	["network-wireless"]="󰑩"
	["phone"]=""
	["printer"]="󰐪"
	["scanner"]="󰚫"
	["unknown"]=""
	["video-display"]=""
)

connected_mark=""
paired_mark=""
start_scan="󱉶 Scan devices"
already_scanning="󱉶 Already scanning..."
turn_off="󰐥 Turn off"
turn_on="󰐥 Turn on"
max_length=25

scanning_script="$HOME/.local/scripts/rofi/bluetooth-controler/bluetooth-scanner.py"
rofi_theme_bluetooth_settings="$HOME/.config/rofi/bluetooth-settings/config.rasi"
rofi_theme_turn_on="$HOME/.config/rofi/turn-on/config.rasi"
devices_cache="/tmp/bluetooth-devices-discovered"
discovery_file="/tmp/bt-is-discovering"


number_blocked=$(LANG=C.UTF-8 rfkill | grep bluetooth | grep ' blocked' | wc -l)
if [ $number_blocked -ge 1 ]; then
	rfkill unblock bluetooth
fi


function get_state {
	bluetooth_status=$(bluetoothctl show)
	power_state=$(echo "$bluetooth_status" | grep "PowerState" | cut -d' ' -f'2')
	discoverable_state=$(echo "$bluetooth_status" | grep "Discoverable" | cut -d' ' -f'2')
	discovering=$(echo "$bluetooth_status" | grep "Discovering" | cut -d' ' -f'2')
}



function get_devices {
	devices_discovered=()
	if [ -f "$devices_cache" ]; then
		cat "$devices_cache" | while read line 
		do
			devices_discovered+="$line"
		done
	fi

	devices_connected=()
	bluetoothctl devices Connected | while read line
	do
		address=$(echo "$line" | cut -d' ' -f'2')
		name=$(echo "$line" | cut -d' ' -f'3-')
		devices_connected+="$address $name"
	done

	devices_paired=()
	bluetoothctl devices Paired | while read line
	do
		address=$(echo "$line" | cut -d' ' -f'2')
		name=$(echo "$line" | cut -d' ' -f'3-')
		dev="$address $name"
		devices_paired+="$dev"
	done
}


function start_scan {
	touch "$discovery_file"
	"$scanning_script" "$devices_cache"
	rm "$discovery_file"
}


function get_info {
	address=$(echo "$1" | cut -d' ' -f'1')
	name=$(echo "$1" | cut -d' ' -f'2-')
	info=$(bluetoothctl info "$address")
	
	if [ $? -eq 0 ]; then
		has_info=true
	else
		has_info=false
	fi

	if [[ $icon_info != "" ]]; then
		icon=$icon_devices[$(echo "$info" | grep "Icon" | cut -d' ' -f'2')]
	elif [[ "$has_info" = true ]]; then
		icon="$icon_active"
	else
		icon="$icon_inactive"
	fi

	is_connected=$(bluetoothctl devices Connected | grep "$address")
	is_connected=$(if [[ "$is_connected" != "" ]]; then; echo "$pos"; else; echo "$neg"; fi)
	is_paired=$(bluetoothctl devices Paired | grep "$address")
	is_paired=$(if [[ "$is_paired" != "" ]]; then; echo "$pos"; else; echo "$neg"; fi)
	is_bonded=$(bluetoothctl devices Bonded | grep "$address")
	is_bonded=$(if [[ "$is_bonded" != "" ]]; then; echo "$pos"; else; echo "$neg"; fi)
	is_trusted=$(bluetoothctl devices Trusted | grep "$address")
	is_trusted=$(if [[ "$is_trusted" != "" ]]; then; echo "$pos"; else; echo "$neg"; fi)
	is_blocked="$neg"
}


function get_main_choices {
	id=0
	if [[ "$power_state" = "on" ]]; then
		main_choices="$turn_off\n"
		if [ -f "$discovery_file" ]; then
			main_choices="$main_choices$already_scanning"
		else
			main_choices="$main_choices$start_scan"
		fi
		for dev in $devices_connected; do
			get_info $dev
			addresses="$addresses\n$id;$address"
			main_choices="$main_choices\n$id $icon  $connected_mark $name"
			id=$(( $id + 1 ))
		done
		for dev in $devices_paired; do
			if [[ ${devices_connected[(r)$dev]} != "$dev" ]]; then
				get_info $dev
				if [[ "${devices_discovered[(r)$dev]}" != "$dev" ]]; then
					icon="$icon_inactive"
				fi
				addresses="$addresses\n$id;$address"
				main_choices="$main_choices\n$id $icon  $paired_mark $name"
				id=$(( $id + 1 ))
			fi
		done
		for dev in $devices_discovered; do
			if [[ "${devices_paired[(r)$dev]}" != "$dev" ]]; then
				get_info $dev
				addresses="$addresses\n$id;$address"
				main_choices="$main_choices\n$id $icon $name"
				id=$(( $id + 1 ))
			fi
		done
	else
		main_choices="$turn_on"
	fi
}


function get_battery_icon {
	battery_line=$(bluetoothctl info "$1" | grep "Battery Percentage")
	if [[ "$battery_line" = "" ]]; then
		has_battery=false
	else
		has_battery=true
	fi

	if [[ "$has_battery" = false ]]; then
		return
	fi
	
	battery=$(echo "$battery_line" | cut -d' ' -f'4' | cut -d'(' -f'2' | cut -d')' -f'1')

	if [ $battery -ge 90 ]; then
		battery_icon="󰥈"
	elif [ $battery -ge 80 ]; then
		battery_icon="󰥆"
	elif [ $battery -ge 70 ]; then
		battery_icon="󰥅"
	elif [ $battery -ge 60 ]; then
		battery_icon="󰥄"
	elif [ $battery -ge 50 ]; then
		battery_icon="󰥃"
	elif [ $battery -ge 40 ]; then
		battery_icon="󰥂"
	elif [ $battery -ge 30 ]; then
		battery_icon="󰥁"
	elif [ $battery -ge 20 ]; then
		battery_icon="󰥀"
	elif [ $battery -ge 10 ]; then
		battery_icon="󰤿"
	else
		battery_icon="󰤾"
	fi
}


get_state
get_devices
if [[ "$1" = "label" ]]; then
	if [[ "$power_state" = "off" ]]; then
		echo "$icon_inactive"
	elif [ "${#devices_connected[@]}" -eq 0 ]; then
		if [ -f "$discovery_file" ]; then
			echo "$icon_scanning scanning..."
		else
			echo "$icon_active"
		fi
	else
		active_connection=$devices_connected[1]
		get_info $active_connection

		get_battery_icon "$address"
		if [ -f "$discovery_file" ]; then
			actual_icon="$icon_scanning"
		elif [[ "$has_battery" = true ]]; then
			actual_icon="$battery_icon"
		else
			actual_icon="$icon_connected"
		fi
		label="$actual_icon $name"

		if [[ ${#label} -gt $max_length ]]; then
			label_length=$(($max_length - 1))
			echo "${label:0:$label_length}…"
		else
			echo "$label"
		fi
	fi
	exit
elif [[ "$1" = "switch" ]]; then
	if [[ "$power_state" = "on" ]]; then
		bluetoothctl power off
	else
		bluetoothctl power on
	fi
	exit
fi


reopen=true
while $reopen; do
	reopen=false
	get_main_choices
	if [[ "$power_state" == "on" ]]; then
		choice=$(echo "$main_choices" | rofi -dmenu -p "󰂳")
	else
		choice=$(echo "$main_choices" | rofi -dmenu -p "󰂳" -config "$rofi_theme_turn_on")
	fi

	if [[ "$choice" == "$turn_off" ]]; then
		bluetoothctl power off
	elif [[ "$choice" == "$turn_on" ]]; then
		bluetoothctl power on
		start_scan
	elif [[ "$choice" == "$start_scan" ]]; then
		start_scan
	elif [[ "$choice" == "$already_scanning" ]]; then
		reopen=true
	elif [[ "$choice" != "" ]]; then
		id=$(echo "$choice" | cut -d' ' -f'1')
		if [[ $(echo "$main_choices" | grep -F "$choice") != "" ]]; then
			dev_address=$(echo "$addresses" | grep "^$id;" | cut -d';' -f'2')
			if [[ "$dev_address" = "" ]]; then
				notify-send "Buetooth controler" "Internal error, option '$choice' not found."
				exit
			fi
		else
			dev_address="$choice"
		fi
		get_info "$dev_address "

		connection_choice="$icons_connection[$is_connected]"
		pairing_choice="$icons_pairing[$is_paired]"
		bonding_choice="$icons_bonding[$is_bonded]"
		trusting_choice="$icons_trusting[$is_trusted]"
		blocking_choice="$icons_blocking[$is_blocked]"
		copy_address=" Copy @MAC"
		choices="$connection_choice\n$pairing_choice\n$trusting_choice\n$copy_address"
		choice=$(echo "$choices" | rofi -dmenu -p "󰂳" -config "$rofi_theme_bluetooth_settings")

		if [[ "$choice" = "$connection_choice" ]]; then
			if [[ "$is_connected" = "$pos" ]]; then
				bluetoothctl disconnect "$address" else
				bluetoothctl connect "$address"
			fi
		elif [[ "$choice" = "$pairing_choice" ]]; then
			if [[ "$is_paired" = "$pos" ]]; then
				bluetoothctl cancel-pairing "$address"
			else
				bluetoothctl pair "$address"
			fi
		elif [[ "$choice" = "$trusting_choice" ]]; then
			if [[ "$is_trusted" = "$pos" ]]; then
				bluetoothctl untrust "$address"
			else
				bluetoothctl trust "$address"
			fi
		elif [[ "$choice" = "$copy_address" ]]; then
			echo -n "$dev_address" | xclip -selection clipboard
		else
			reopen=true
		fi
		if [ $? -eq 1 ]; then
			notify-send "Bluetooth controler" "Device of address '$address' unreachable."
		fi
	fi
done
