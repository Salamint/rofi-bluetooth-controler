# rofi bluetooth controler

This small script of about 300 or more lines allows anyone to use [rofi](https://github.com/davatorium/rofi) as a way to __control and scan__ bluetooth devices in the area around you.

It comes with the ability to:
- Turn on and off the bluetooth on your system
- Scan and list devices in your area
- List all paired devices
- Connect/disconnect, pair/unpair and trust/untrust devices listed
- Cache the scanned devices in the `/tmp/` directory (deleted at restart) Copy easily the `MAC` address of any device listed

It can also be bound to a keybinding on a window manager such as [i3](https://i3wm.org/) to be started when a combination of keys is stroke.
It can also be incorporated in a status bar like [polybar](https://polybar.github.io/) as a module, and disply to which bluetooth device you are connected too, as well as an icon to indicate the battery percentage (if the battery is available) and if the script is scanning for new devices.
And if connected to no device, an icon showing if bluetooth is enabled or disabled.

## Important notes

### Python sub script
The python script **NEEDS TO BE LOCATED TO `$scanning_script`**, otherwise, the controler will still be able to interact with listed devices and turn on/off the bluetooth normally, but it will not be able to scan for devices, and it might even break the status bar.

It is however possible to fix this behaviour by changing the `scanning_script` at the begining of the `zsh` script, and make it point to the new location of scanning script (you can replace the shipped script for scanning with your own implementation).

### rfkill unblocking
It can happen that sometimes, the bluetooth by default is software blocked. You might have ecountered this issue where you are trying to turn on the bluetooth, but it keeps throwing an error.
The reason is often that on your computer, the bluetooth is blocked by software by default. You can check this by running `rfkill` (for more information, please refer to [this page](https://wiki.archlinux.org/title/Bluetooth) of the Arch wiki).

This script does the job for you by checking if the bluetooth is blocked, and unblocking it automatically. If this behaviour does not please you, consider editing the script. The section that unblocks the bluetooth is somewhere at the begining of the file.

### rofi themes
This script will try to use some custom configuration files for rofi, this allows you to customize the appearence of the "turn on" mode, and the "settings mode".
If these files doesn't exist, `rofi` will use your default configuration for all modes.
But you can edit the script to select manually the location of your rofi config file.

## Table of Contents

1. [Requirements](#requirements)
    1. [zsh](#zsh)
    2. [Python 3.11](#python-311)
    3. [rofi](#rofi)
    4. [bluez](#bluez)
    5. [Nerd font](#nerd-font)
2. [Usage](#usage)
    1. [In a terminal](#in-a-terminal)
    2. [Main features](#main-features)
    3. [Managing a device](#managing-a-device)
4. [Icons meaning](#icons-meaning)
    1. [Status](#status)
    2. [Device list](#device-list)
5. [Integration](#integration)
    1. [Polybar module](#polybar)
    2. [Add a bindsym for i3 (or any tiling WM)](#add-a-bindsym-for-i3-or-any-tiling-wm)
6. [To-do](#to-do)

## Requirements

### zsh
This script is written in the `zsh` shell script language, so you will need to have `zsh` installed on your computer. Although you do not need to explicitely call `zsh` when executing the script, it contains a `#!/bin/zsh` line at the begining of the file.

### Python 3.11
This shell script uses a `python` script to scan available devices. So you will need to have Python version 3.11 or above installed. This shouldn't be an issue because most Linux distribution comes with a Python installation, usually 3.11.

### rofi
And of course, the main requirement is [rofi](https://github.com/davatorium/rofi). If you are on this page, you should already know about what this program does and you probably already have it installed.

### bluez
You will need a bluetooth daemon running in the background ready to manage your bluetooth to use this script.
The standard option when it comes to bluetooth on Linux is `bluez` which comes with a bluetooth dameon.
You will also need `bluez-tools` which comes with `bluetoothctl`, on which this script depends. `bluetoothctl` is command line utility for managing the `bluez` bluetooth daemon.

### Nerd font
If you want to be able to see the icons used in the rofi menu (which helps a lot, because each icon has a meaning).

## Usage

### In a terminal
This script can be ran from the terminal, but it is not a command line utility. So there is no integrated help for now, and only 2 "commands" are available at this time.

First, running the script directly with no argument will simply open the rofi menu. This can still be interesting if you need to see some of the output in the terminal.

Calling the script with the `switch` argument will just turn on/off the bluetooth, depending on which state it was before. This will not open any rofi menu and won't print anything.

Calling the script with the `label` argument will write in the standard output an icon, indicating the state of the bluetooth, as well as the label of the device you are connected too (if any).

### Main features
This script comes with many features for managing bluetooth devices. The most basic feature is turning on and off the bluetooth. When your bluetooth is turned off, the rofi menu will have only one option available : `turn on`. You can cancel the script at any time using the escape key.

When turned on, the rofi menu will show a lot more options. Firstly, the possibility to turn off the bluetooth. In second you will be able to scan for available devices in the area. The scanning delay depends on the scanning sub script.

Finally, when bluetooth is on, you can have a list of devices you can manage. At the top (fill stars), you have devices you are connected to.
Below them you have paired devices (empty stars). Those are available at all times, this way you can try to connect to a paired device even if it is not nearby, or unpair a paired device without having it around.
And at the bottom, you have a list of all discovered devices (if you have started a scan, or cached a scan, which is automatic). You can then try connecting to one of them.

### Managing a device
After selecting a device in the list, a submenu opens with new options. You can close the submenu by clicking outside the rofi window or by pressing `escape`.

The 3 main options availables are:
- Connecting/disconnecting to a device (initiating a connection with a device).
- Paring/unpairing a device (saving the device's credentials so you connect easier next time).
- Trusting/untrusting a device (i won't explain what this means but you can do it, just know it).

You also have the possibility to copy the `Mac` address of a device to your clipboard. This comes very handy when the script has an issue and so you can use `bluetoothctl` without having to search for the address of the device.

## Icons meaning

### Status
| Icon | Meaning |
|:----:|---------|
| `󰂲` | Bluetooth turnes off. |
| `󰂯` | Bluetooth turnes on. |
| `󰂰` | Scanning for devices. |
| `󰂱` | Connected to a device. |
| `󰥄` | Device's battery. |

### Device list
| Icon | Meaning |
|:----:|---------|
| ``  | Connected device. |
| ``  | Paired device. |
| `󰂯`  | Device Nearby. |
| `󰂲`  | Device not detected (possibly discovered some time ago). 
| ``  | Device of type audio card (`audio-card`) |
| ``  | Device of type audio output device (`audio-headset`) |
| ``  | Device of type headphones (`audio-headphones`) |
| `󰄀`  | Device of type photo camera (`camera-photo`) |
| ``  | Device of type video camera (`camera-video`) |
| `󰇅`  | Device of type computer (`computer`) |
| `󰊴`  | Device of type gaming input (`input-gaming`) |
| `󰌌`  | Device of type keayboard (`input-keyboard`) |
| `󰍽`  | Device of type mouse (`input-mouse`) |
| `󰦧`  | Device of type tablet (`input-tablet`) |
| `󰀂`  | Device of type modem (`modem`) |
| `󰎇`  | Device of type multimedia player (`multimedia-player`) |
| `󰑩`  | Device of type wireless network device (`network-wireless`) |
| ``  | Device of type phone (`phone`) |
| `󰐪`  | Device of type printer (`printer`) |
| `󰚫`  | Device of type scanner (`scanner`) |
| ``  | Device of type unknown (`unknown`) |
| ``  | Device of type video display (`video-display`) |

## Integration

### Polybar module
To integrate this script to your polybar as a module, you are goin to create a module inside your polybar configuration file, like so:
```ini
[module/bluetooth]
type = custom/script

format = <label>
label = %output%
interval = 0.5

exec = $SCRIPTS_DIR/bluetooth-controler.sh label
click-left = $SCRIPTS_DIR/bluetooth-controler.sh
double-click-left = $SCRIPTS_DIR/bluetooth-controler.sh switch
```
Where `$SCRIPTS_DIR` is any directory where is stored the script file.

After that, you must add the module to one of your bars:
```ini
[bar/example]
inherit = base-bar

modules-left = bluetooth
```

### Add a bindsym for i3 (or any tiling WM)
You can bind the bluetooth controler to shortcut in your window manager to open it almost instantly without having to use the mouse ever. In my case i use i3.
You can add this line to your i3 config file to open the bluetooth controler when pressing `$mod+b`, where `$mod` is your modifier key:
```
bindsym $mod+b exec --no-startup-id "$SCRIPT_DIR/bluetooth-controler.sh"
```
This assuming you have no other shortcuts bound to `$mod+b`. Feel free to customize the shortcut as you desire.

## To-do

There is still a lot of bugs to be fixed and features to add, so please consider any issue you are encountering, and make a pull request for any fix you made.
There is also some improvements to be made, this script can be pretty slow and readibility is not very good.
So if you have some free time and you are enjoying this script, consider suggest some improvements or ideas of features, or even better, cntribute to this small project.
